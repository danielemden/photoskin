//
//  String+Extensions.swift
//  PhotoSkin
//
//  Created by Daniel Emden on 17.10.18.
//  Copyright © 2018 Institut für Medizinische Informatik. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
