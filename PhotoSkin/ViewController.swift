//
//  ViewController.swift
//  PhotoSkin
//
//  Created by Daniel Emden on 17.10.18.
//  Copyright © 2018 Institut für Medizinische Informatik. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var patientIdTextField: UITextField!
    @IBOutlet weak var bodyPartTextField: UITextField!
    @IBOutlet var pickImageButton: UIButton!
    
    let imagePicker = UIImagePickerController()
    let pickerView = UIPickerView()
    let bodyParts = ["head", "torso", "arms", "legs"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup() {
        // image picker
        imagePicker.delegate = self
        
        // image picker button
        pickImageButton.addTarget(self, action: #selector(pickImageAction(sender:)), for: .touchUpInside)
        
        // picker view
        pickerView.backgroundColor = .white
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.reloadAllComponents()
        pickerView.showsSelectionIndicator = true
        
        // picker toolbar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(donePickerAction))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(donePickerAction))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        bodyPartTextField.inputView = pickerView
        bodyPartTextField.inputAccessoryView = toolBar
    }
    
    @objc func donePickerAction() {
        bodyPartTextField.resignFirstResponder()
    }
    
    @objc func pickImageAction(sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        } else {
            print("camera not available")
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            print("yay" + "\(pickedImage.size.width)")
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UINavigationControllerDelegate and DataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bodyParts.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return bodyParts[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        bodyPartTextField.text = bodyParts[row]
    }
    
}

